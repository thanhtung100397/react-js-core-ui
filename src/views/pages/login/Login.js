import React from 'react'
import { Link } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm, CFormGroup,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText, CInvalidFeedback,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { logInByUsernamePassword } from "../../../services/LoginService";

const Login = (props) => {
  const [validated, setValidated] = React.useState(false);
  const [username, setUsername] = React.useState();
  const [password, setPassword] = React.useState();

  const handleSignInFormSubmit = async (event) => {
    if (!event.target.checkValidity()) {
      setValidated(true);
    }
    try {
      let res = await logInByUsernamePassword(username, password);
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="8">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm noValidate wasValidated={validated} onSubmit={(event) => handleSignInFormSubmit(event)}>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <CFormGroup>
                      <CInputGroup className="mb-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-user" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput type="text"
                                placeholder="Username" autoComplete="username"
                                onChange={(event) => setUsername(event.target.value)}
                                required />
                        <CInvalidFeedback className="help-block">Please enter your username</CInvalidFeedback>
                      </CInputGroup>
                    </CFormGroup>
                    <CFormGroup>
                      <CInputGroup className="mb-4">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-lock-locked" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput type="password"
                                placeholder="Password" autoComplete="current-password"
                                onChange={(event) => setPassword(event.target.value)}
                                required />
                        <CInvalidFeedback className="help-block">Please enter your password</CInvalidFeedback>
                      </CInputGroup>
                    </CFormGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton type="submit" color="primary" className="px-4">Login</CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0">Forgot password?</CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                <CCardBody className="text-center">
                  <div>
                    <h2>Sign up</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                      labore et dolore magna aliqua.</p>
                    <Link to="/register">
                      <CButton color="primary" className="mt-3" active tabIndex={-1}>Register Now!</CButton>
                    </Link>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
