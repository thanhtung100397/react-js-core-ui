import {api} from "../config/api/ApiClient";

const clientID = process.env.REACT_APP_CLIENT_ID;
const clientSecret = process.env.REACT_APP_CLIENT_SECRET;

export const logInByUsernamePassword = (username, password) => {
  return api({
    method: 'POST',
    url: '/api/oauth2/authentication/username-password',
    data: {
      username: username,
      password: password
    },
    headers: {
      client_id: clientID,
      secret: clientSecret
    }
  });
}
