
export const ACCESS_TOKEN = "access_token";
export const REFRESH_TOKEN = "refresh_token";

export const logLevels = {
  INFO: 'info',
  DEBUG: 'debug',
  ERROR: 'error'
}

export const routes = {
  LOGIN: '/login',
  REGISTER: '/register',
  HOME: '/',
};

export const INVALID_TOKEN_STATUS_CODES = [404];
export const INVALID_TOKEN_CODES = [4014, 4015];
