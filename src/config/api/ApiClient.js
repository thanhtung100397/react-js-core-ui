import axios from 'axios';
import { ACCESS_TOKEN, routes, INVALID_TOKEN_STATUS_CODES, INVALID_TOKEN_CODES } from '../../constants/constants';
import { logError, logInfo } from '../../utils/log/Logger'

const requestInstant = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL
})

// On Each Request Interceptor
requestInstant.interceptors.request.use(
  (config) => {
    logRequest(config);
    return config;
  },
  (error) => {
    logError(error);
    Promise.reject(error);
  }
);

// On Each Response Interceptor
requestInstant.interceptors.response.use(
  (response) => {
    logResponse(response);
    return response;
  },
  (error) => {
    if (error.response) {
      if (INVALID_TOKEN_CODES.includes(error.response.status)) {
        let body = error.response.data;
        if (body && INVALID_TOKEN_STATUS_CODES.includes(body.code)) {
          window.location.href = routes.LOGIN;
        }
      }
      logResponse(error.response);
    } else {
      logError(error);
    }
    return Promise.reject(error);
  }
);

const logRequest = (reqConfig) => {
  if (reqConfig) return;
  logInfo({
    request: {
      url: `${reqConfig.method} ${reqConfig.baseURL}${reqConfig.url}`,
      params: reqConfig.params,
      body: reqConfig.data,
      headers: reqConfig.headers
    }
  });
}

const logResponse = (response) => {
  if (!response) return;
  let reqConfig = response.config;
  logInfo({
    response: {
      url: `${reqConfig.method} ${reqConfig.baseURL}${reqConfig.url}`,
      params: reqConfig.params,
      status: `${response.status} ${response.statusText}`,
      body: response.data,
      headers: response.headers
    }
  });
}

export const api = (options = {}) => {
  return requestInstant.request({
    ...options
  });
};

export const apiAuth = (options = {}) => {
  return requestInstant.request({
    ...options,
    headers: {
      Authorization: 'Bearer ' + localStorage.getItem(ACCESS_TOKEN),
      ...options.headers,
    }
  });
};
