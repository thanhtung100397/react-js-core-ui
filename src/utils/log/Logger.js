import { logLevels } from "../../constants/constants";

export const isLogLevelEnabled = (level) => {
  if (!level) {
    return false;
  }
  return process.env.REACT_APP_ENABLE_LOG_LEVEL.includes(level.toLowerCase());
}

export const logInfo = (data) => {
  if (isLogLevelEnabled(logLevels.INFO)) {
    console.info(data);
  }
}

export const logDebug = (data) => {
  if (isLogLevelEnabled(logLevels.DEBUG)) {
    console.debug(data);
  }
}

export const logError = (data) => {
  if (isLogLevelEnabled(logLevels.ERROR)) {
    console.error(data);
  }
}
